import PdfOrbitComponent from './PdfOrbit.svelte';

import { Plugin } from '../node_modules/foundation-sites/js/foundation.core.plugin.js';

/**
 * Orbit module.
 * @module foundation.pdforbit
 */
class PdfOrbit extends Plugin {
    /**
     * Creates a new instance of PdfOrbitPlugin
     * @class
     * @name PdfOrbitPlugin
     * @param {jQuery} element - jQuery object to make into an PdfOrbitPlugin
     * @param {Object} options - Overrides to the default plugin settings
     */
    _setup(element, options) {
        this.$element = element;
        this.options = Object.assign({}, PdfOrbit.defaults, this.$element.data(), options);
        this.className = 'PdfOrbit'; // ie9 back compat

        this._init();
    }

    /**
     * Initializes the plugin
     * @function
     * @private
     */
    _init() {
        const el = this.$element[0];
        this.app = new PdfOrbitComponent({
            target: el,
            props: { src: el.dataset.pdfUrl }
        });
    }
}

PdfOrbit.defaults = {
    scale: 2.0
};

Foundation.plugin(PdfOrbit, 'PdfOrbit');

export { PdfOrbit };
