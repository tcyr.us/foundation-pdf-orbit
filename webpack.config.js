const path = require('path');
const MinifyPlugin = require('babel-minify-webpack-plugin');

module.exports = {
    mode: 'production',
    entry: {
        'foundation.pdforbit': './src/foundation.pdforbit.js',
        'foundation.pdforbit.min': './src/foundation.pdforbit.js'
    },
    resolve: {
        alias: {
            svelte: path.resolve('node_modules', 'svelte')
        },
        extensions: ['.mjs', '.js', '.svelte'],
        mainFields: ['svelte', 'browser', 'module', 'main']
    },
    externals: {
        jquery: 'jQuery',
        Foundation: 'Foundation',
    },
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: '[name].js'
    },
    module: {
        rules: [
            {
                test: /\.m?js$/,
                include: [/svelte/],
                use: 'babel-loader'
            },
            {
                test: /\.svelte$/,
                use: [
                    'babel-loader',
                    'svelte-loader'
                ]
            },
        ]
    },
    plugins: [
        new MinifyPlugin({}, {
            test: /\.min\.js$/,
            comments: false
        })
    ],
    devtool: 'inline-source-map',
};
